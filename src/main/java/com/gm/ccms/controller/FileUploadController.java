package com.gm.ccms.controller;

import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.file.Paths;

@RestController
@RequestMapping("/file/upload")
public class FileUploadController {


    @PostMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE,MediaType.APPLICATION_JSON_VALUE})
    public Mono<String> uploadFile(@RequestPart("file") Flux<FilePart> filePartFlux){
         return filePartFlux.flatMap(file -> file.transferTo(Paths.get("/home/vagrant/code/" + file.filename()))).then(Mono.just("OK"));

    }

}
