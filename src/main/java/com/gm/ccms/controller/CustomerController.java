package com.gm.ccms.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/customer/api/v1")
public class CustomerController {

	@GetMapping(value = "/getCustomer")
	public ResponseEntity<Response<Customer>> getData(ServerHttpRequest request, ServerHttpResponse response) {
		System.out.println("Inside get Customer ");
		HttpHeaders headers = request.getHeaders();

		headers.forEach((k,v)->{
			System.out.println(k + " : " + v);
		});

		ResponseCookie.ResponseCookieBuilder builder = ResponseCookie.from("nt-ms1-cookie", "ntMs1CookieValue");
		ResponseCookie cookie = builder.build();
		response.addCookie(cookie);
		Response<Customer> serviceResponse = new Response<>();
		Customer customer = new Customer();
		customer.setEmail("nilesh.rao@gmail.com");
		customer.setName("Nilesh Rao");
		serviceResponse.setResponse(customer);
		return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
	}
}